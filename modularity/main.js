(function() {
    var width = 1900 / 1.5;
    var height = 1080 / 1.5;

    //var color = d3.scale.category20();
    var color = d3.scale.ordinal()
        .domain([1, 5, 10, 20, 35])
        .range(["#631413", "#AE2521", "#DD442B", "#E87130", "#F9DA6C"]);

    var force = d3.layout.force()
        .charge(-50)
        .linkDistance(50)
        .size([width, height]);

    var svg = d3.select("body").append("svg")
        .attr("width", width)
        .attr("height", height);

    d3.json("data.json", function(error, graph) {
        if (error) throw error;

        force
            .nodes(graph.nodes)
            .links(graph.links)
            .start();

        var link = svg.selectAll(".link")
            .data(graph.links)
            .enter().append("line")
            .attr("class", "link")
            .style("stroke-width", function(d) {
                return Math.sqrt(d.value) / 10.0;
            });

        var node = svg.selectAll(".node")
            .data(graph.nodes)
            .enter().append("circle")
            .attr("class", "node")
            .attr("cx", width / 2)
            .attr("cy", height / 2)
            .attr("r", function(d) {
                return (d.size + 9) / 4;
            })
            .style("fill", function(d) {
                return color(d.group);
            })
            // <!--.size(function(d) { return color(d.group); })-->
            .call(force.drag);

        node.append("title")
            .text(function(d) {
                return d.name;
            });

        node.append("text")
            .attr("dx", 12)
            .attr("dy", ".35em")
            .text(function(d) {
                return d.size;
            });

        force.on("tick", function() {
            link.attr("x1", function(d) {
                    return d.source.x;
                })
                .attr("y1", function(d) {
                    return d.source.y;
                })
                .attr("x2", function(d) {
                    return d.target.x;
                })
                .attr("y2", function(d) {
                    return d.target.y;
                });

            node.attr("cx", function(d) {
                    return d.x;
                })
                .attr("cy", function(d) {
                    return d.y;
                });
        });
    });
})();
